#!/usr/bin/env node

var fileSystem = require('fs');


let dectionary = new Map();

//read from the file
var readFromTheFile= function() {
    fileSystem.readFile('dectionary.txt', (err,data) => {
      if (err) throw err;
          dectionary=  new Map(JSON.parse(data));
    });
}
//write to the file
var writeToTheFile= function() {
    fileSystem.writeFile('dectionary.txt', JSON.stringify([...dectionary]), (err) => {
      if (err) throw err;
          return true;
    });
}
//delay for async functions (read and write)
function wait(ms) {
  return new Promise(r => setTimeout(r, ms));
}
//the async functions
async function asyncWrite() {
  writeToTheFile();
  await wait(10);
}
async function asyncRead() {
  readFromTheFile();
  await wait(10);
}


module.exports = {
    add: async function(key,value){
        await asyncRead(); //read the dectionary from file
        dectionary.set(key,value); // add the key to the dectionary
        writeToTheFile(); // write it back
        return key +" has been added"

    }, list: async function(){
        await asyncRead();  //read the dectionary from file
        for (let key of dectionary.keys()) { //iterate on the list list
            console.log(key + " -> " + dectionary.get(key));
        }
        if (dectionary.size == 0)
        return "The dectionary is empty"
        return ""

    }, get: async function(key){
        await asyncRead();
          return key + " -> " + dectionary.get(key);

    }, remove: async function(key){
        await asyncRead();
        dectionary.delete(key);
        writeToTheFile();
        return key + 'has been deleted';

    }, clear: async function(){
        dectionary.clear();
        await writeToTheFile();
        return "The dictionary has been cleared";

    },
};





require('make-runnable/custom')({
    printOutputFrame: false
});
